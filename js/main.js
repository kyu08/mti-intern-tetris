document.getElementById("hello_text").textContent = "はじめてのJavaScript";

// テトリミノなど定義

var countSecond = 0;

var cells;

var blocks = {
    i: {
        class: "i",
        pattern: [
            [1,1,1,1]
        ] 
    },
    o: {
        class: "o",
        pattern:[
            [1, 1], 
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern:[
            [0, 1, 0], 
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern:[
            [0, 1, 1], 
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern:[
            [1, 1, 0], 
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern:[
            [1, 0, 0], 
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern:[
            [0, 0, 1], 
            [1, 1, 1]
        ]
    }
};

// 処理実行部分
//  カウント増やす＋落とす＋揃った行を消す＋ゲームオーバー判定＋つくる
loadTable();
// wholeOperation();


//  キー入力で左右に移動 
document.addEventListener("keydown", onKeyDown);


// 関数定義
// function wholeOperation(){
    var gameOperation = setInterval(function(){
        countUpTitle();
        if(hasFallingBlock()){
            fallBlocks();
        }else{
            deleteRow();
            // ゲームオーバー判定＋処理
            gameOver();
            generateBlock();
        }
    },1000);
// }

function countUpTitle(){
    countSecond++;
    
    var count = countConversion(countSecond);
    // todo countを分秒で表示する
    document.getElementById("hello_text").textContent = "Tetris®︎("+ count + ")";
}

function countConversion(countSecond){
    var hour = Math.floor(countSecond / 60 / 60);
    // Math.floor(countSecond / 60 * 60);
    var minute = Math.floor(countSecond / 60);
    var second = countSecond % 60;
    var result = hour + ":" + minute + ":" + second;
    return result;
}



function loadTable(){
    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for(var row = 0; row < 20; row++){
        cells[row] = [];
        for(var col = 0; col < 10; col++){
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

function fallBlocks(){
    // 1.底に到達しているかどうか
    for (var col = 0; col < 10; col++) {
        if (cells[19][col].blockNum === fallingBlockNum) {
            isFalling = false;
            return;
        }
    }

    // 2.真下にブロックがあるかどうか
    for (var row = 18; row >= 0; row--) {
        for (var col = 0; col < 10; col++){
            if (cells[row][col].blockNum === fallingBlockNum ) {
                if (cells[row+1][col].className !== "" && cells[row+1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    return;
                }
            }
        }
    }

    // 1.も2.もfalseならば(下に何もないなら)落とす
    for (var row = 18; row >=0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row+1][col].className = cells[row][col].className;
                cells[row+1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;
function hasFallingBlock(){
    return isFalling;

}

function deleteRow(){
    var canDelete = true;
    for (var row = 19; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].className === "") {
                canDelete = false;
                break;
            }
        }
    
        if (canDelete) {
            // けす
            for (var col = 0; col < 10; col++) {
                // cells[row][col].className = "";
                // cells[row][col].fallingBlockNum = null;
                cells[row][col].className = cells[row - 1][col].className;
                cells[row][col].blockNum = cells[row -1][col].blockNum;
                cells[row-1][col].className = "";
                cells[row-1][col].fallingBlockNum = null;
            }
        }
    }
}

var fallingBlockNum = 0;
function generateBlock(){
    // 次のブロックを指定
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    // それを生成してセット
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) { 
                cells[row][col+3].className = nextBlock.class;
                cells[row][col+3].blockNum = nextFallingBlockNum;
            }
        }
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
    }
}

// ゲームオーバー判定
function gameOver(){
    for (var col = 0; col < 10; col++) {
        if (cells[0][col].className !== ""){
            alert("GameOver!");
            document.getElementById("hello_text").textContent = "GameOver!";
            clearInterval(gameOperation);
        }
    }

}

//  キー操作
function onKeyDown(event){
    if (event.keyCode === 37) {
        moveLeft();
    }else if (event.keyCode === 39) {
        moveRight();
    }else if (event.keyCode === 40) {
        moveDown();
    }
}

function moveRight(){
    for (var row = 0; row < 20; row++) {
        for (var col = 9; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
                // 右へうつす
            }
        }
    }
}

function moveLeft(){
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveDown(){
    // for (var row = 0; row <　20; row++) {
    //     for(var col = 0; col < 10; col++) {
    //         if (cells[row][col].blockNum === fallingBlockNum) {
    //             cells[row - 1][col].className = cells[row][col].className;
    //             cells[row - 1][col - 1].blockNum = cells[row][col].blockNum;
    //             cells[row][col].className = "";
    //             cells[row][col].blockNum = null;
    //         }
    //     }
    // }
}

